package com.example.waterfall.testlivechatinc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.livechatinc.inappchat.ChatWindowConfiguration;
import com.livechatinc.inappchat.ChatWindowView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startFullScreenChat();
    }

    public void startFullScreenChat() {

        Intent intent = new Intent(this, com.livechatinc.inappchat.ChatWindowActivity.class);
        intent.putExtra(com.livechatinc.inappchat.ChatWindowActivity.KEY_GROUP_ID, "0");
        intent.putExtra(com.livechatinc.inappchat.ChatWindowActivity.KEY_LICENCE_NUMBER, "10283022");
        startActivity(intent);
    }
}
